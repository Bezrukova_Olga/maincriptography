package com.vsu.springcriptography.utils;

import com.vsu.springcriptography.dto.Message;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class PermutationEncryption {

    public String main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите тип операции (1) - кодирование (2) - раскодирование: ");
        String operation = args[0];
        if (operation.length() == 1 && (operation.equals("1") || operation.equals("2"))) {
            System.out.println("Кодируемая строка: ");
            String s = args[1];

            int sizeSlb = (int) Math.sqrt(s.length());
            int sizeStr = (int) Math.ceil((double) s.length() / sizeSlb);
            if (operation.equals("1")) {
                if ((double) s.length() % sizeSlb != 0) {
                    while ((double) s.length() % sizeSlb != 0) {
                        s += " ";
                    }
                }
                System.out.println("Ключ размерности " + sizeStr + ": ");
                String w = args[2];
                int[] symbols = getSymbols(w);
                System.out.println("Ключ размерности " + sizeSlb + ": ");
                String w1 = args[3];
                int[] cipher = getSymbols(w1);
                try {
                    if (symbols.length == sizeStr && maxValue(symbols) && repeat(symbols) && cipher.length == sizeSlb && maxValue(cipher) && repeat(cipher)) {
                        String coder = coder(s, symbols, cipher, sizeStr, sizeSlb);
                       return coder;
                    } else {
                        return "Error! Incorrect value.";
                    }
                } catch (NullPointerException e) {
                    return "Ошибка ввода";
                }
            } else if (operation.equals("2") && sizeSlb*sizeStr == s.length()) {
                System.out.println("Введите шифр строк " + sizeStr + ": ");
                String str = args[2];
                int[] symbols = getSymbols(str);
                System.out.println("Введите шифр столбцов " + sizeSlb + ": ");
                String str1 = args[3];
                int[] symbols1 = getSymbols(str1);
                if (symbols.length == sizeStr && symbols1.length == sizeSlb && maxValue(symbols) && maxValue(symbols1) && repeat(symbols) && repeat(symbols1)) {
                    return "Расшифрованная строка: " + decoder(s, getSymbols(str), getSymbols(str1), sizeStr, sizeSlb);
                } else {
                    return "Вы ввели неверный шифр.";
                }
            } else {
                return "Error";
            }
        } else {
            return "Повторите ввод";
        }
    }

    private static int[] getSymbols(String line) {
        String line1 = line.replaceAll("[\\s]{2,}", " ");
        String[] s = line1.split(" ");
        try {
            int[] array = new int[s.length];
            for (int i = 0; i < s.length; i++) {
                array[i] = Integer.parseInt(s[i]);
            }
            return array;
        } catch (NumberFormatException e) {
            System.out.println("Ошибка ввода");
            return null;
        }
    }

    private static boolean maxValue(int[] array) {
        int max = -1;
        for (int i = 0; i < array.length; i++) {
            if (!(array[i] < 100 && array[i] > 0))
                return false;
        }
        for (int i = 0; i < array.length; i++) {
            if (array[i] > max)
                max = array[i];
        }
        if (array.length != max) {
            return false;
        } else {
            return true;
        }
    }

    private static boolean repeat(int[] array) {
        Integer[] arrayInt = new Integer[array.length];
        for (int i = 0; i < array.length; i++) {
            arrayInt[i] = new Integer(array[i]);
        }
        Set<Integer> unique = new HashSet<Integer>(Arrays.asList(arrayInt));
        if (unique.size() == array.length) {
            return true;
        } else {
            return false;
        }
    }

    private static String[] getSymbolsString(String line) {
        String[] array = new String[line.length()];
        for (int i = 0; i < line.length(); i++) {
            array[i] = Character.toString(line.charAt(i));
        }
        return array;
    }

    private static String coder(String s, int[] cipher, int[] cipherStlb, int str, int slb) {
        String[][] coder = new String[str][slb];
        int j = 0;
        int k = slb;
        for (int i = 0; i < cipher.length; i++) {
            coder[cipher[i] - 1] = getSymbolsString(s.substring(j, k));
            j += slb;
            k += slb;
        }
        for (int i = 0; i < str; i++) {
            for (int l = 0; l < slb; l++) {
                System.out.print(coder[i][l] + " ");
            }
            System.out.println();
        }
        String result = "";
        for (int i = 0; i < cipherStlb.length; i++) {
            for (int l = 0; l < str; l++) {
                result += coder[l][cipherStlb[i] - 1];
            }
        }
        System.out.println("Result: " + result);
        return result;
    }

    private static String decoder(String s, int[] cipher, int[] cipherStlb, int str, int slb) {
        String[][] decoder = new String[str][slb];
        int j = 0;
        for (int i = 0; i < cipherStlb.length; i++) {
            for (int l = 0; l < str; l++) {
                decoder[l][cipherStlb[i] - 1] = Character.toString(s.charAt(j));
                j++;
            }
        }
        for (int i = 0; i < str; i++) {
            for (int l = 0; l < slb; l++) {
                System.out.print(decoder[i][l] + " ");
            }
            System.out.println();
        }
        String result = "";
        for (int i = 0; i < cipher.length; i++) {
            for (int k = 0; k < slb; k++) {
                result += decoder[cipher[i] - 1][k];
            }

        }
        System.out.println("Result: " + result);
        return result;
    }

}
