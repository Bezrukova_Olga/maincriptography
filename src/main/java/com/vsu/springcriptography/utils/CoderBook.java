package com.vsu.springcriptography.utils;// Кодировочная книга

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class CoderBook {
    public String main(String[] args) {
        Map<String, List<String>> coderBook = readFromFile();
        for (Map.Entry<String, List<String>> entry : coderBook.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue().toString());
        }
        System.out.println(coderBook.size());
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter string: ");
        String inputLine = args[0];
        System.out.println("Please, enter operation (1) - coder, (2) - decoder: ");
        String operation = args[1];
        if (operation.equals("1")) {
            inputLine = deleteSpace(inputLine);
            String[] s = inputLine.split(" ");
            String coder = coder(s, coderBook);
            if (!coder.equals("")) {
                return coder;
            } else {
                return "Change coder book, please";
            }
        } else if (operation.equals("2")) {
            String line = decoder(inputLine, coderBook);
            if (!line.equals(""))
                return line;
            else
                return "Error with decode";
        } else {
            return "Incorrect operation. Try again!";
        }


    }

    private static Map<String, List<String>> readFromFile() {
        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader("CoderBook.txt"));
            String line;
            List<List<String>> coderBook = new ArrayList<>();
            int i = 0;
            while ((line = bufferedReader.readLine()) != null) {
                String[] s = line.split(" ");
                for (int ii = 0; ii < s.length; ii++) {
                    System.out.println(s[ii]);
                }
                coderBook.add(i, Arrays.asList(s));
                i++;
            }
            bufferedReader.close();
            Map<String, List<String>> map = createMap(coderBook);
            return map;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

    }

    private static Map<String, List<String>> createMap(List<List<String>> coderBook) {
        Map<String, List<String>> book = new LinkedHashMap<>();
        int max = -1;
        for (List<String> str : coderBook)
            for (String stlb : str) {
                if ((Integer.toString(coderBook.indexOf(str)) + Integer.toString(str.indexOf(stlb))).length() > max) {
                    max = str.size();
                }
            }
        int i = -1;
        int j;
        for (List<String> str : coderBook) {
            j = -1;
            i++;
            for (String stlb : str) {
                j++;
                if ((Integer.toString(i) + Integer.toString(j)).length() < max) {
                    List<String> add = new ArrayList<>();
                    if (book.containsKey(stlb)) {
                        for (Map.Entry<String, List<String>> entry : book.entrySet()) {
                            if (entry.getKey().equals(stlb)) {
                                entry.getValue().add(defineLength(Integer.toString(i) + Integer.toString(j), max));
                            }
                        }
                    } else {
                        add.add(defineLength(Integer.toString(i) + Integer.toString(j), max));
                        book.put(stlb, add);
                    }
                } else {
                    List<String> add = new ArrayList<>();
                    if (book.containsKey(stlb)) {
                        for (Map.Entry<String, List<String>> entry : book.entrySet()) {
                            if (entry.getKey().equals(stlb)) {
                                entry.getValue().add(Integer.toString(i) + Integer.toString(j));
                            }
                        }
                    } else {
                        add.add(Integer.toString(i) + Integer.toString(j));
                        book.put(stlb, add);
                    }

                }
            }
        }
        return book;
    }

    private static String deleteSpace(String input) {
        return input.replaceAll("[\\s]{2,}", " ");
    }

    private static String coder(String[] coder, Map<String, List<String>> coderbook) {
        String outputLine = "";
        int remember = 0;
        String[] duplicate = new String[coder.length];
        for (int i = 0; i < coder.length; i++) {
            if (duplicate[i] == null) {
                if (coderbook.containsKey(coder[i])) {
                    List<Integer> integers = countWord(coder, coder[i]);
                    for (Map.Entry<String, List<String>> entry : coderbook.entrySet()) {
                        if (entry.getKey().equals(coder[i])) {
                            int k = 0;
                            for (int j = 0; j < integers.size(); j++) {
                                if (k > entry.getValue().size() - 1) {
                                    k = 0;
                                }
                                duplicate[integers.get(j)] = entry.getValue().get(k);
                                k++;
                            }
                        }
                    }
                } else {
                    return "";
                }
            }
        }
        for (int i = 0; i < duplicate.length; i++) {
            outputLine += duplicate[i];
        }
        return outputLine;
    }


    private static List<Integer> countWord(String[] words, String check) {
        List<Integer> v = new ArrayList<>();
        for (int i = 0; i < words.length; i++) {
            if (words[i].equals(check)) {
                v.add(i);
            }
        }
        return v;
    }

    private static String decoder(String decoder, Map<String, List<String>> coderbook) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter length of block: ");
        try {
            int length = 3;
            String[] strings = parseString(decoder, length);
            String outputDecode = "";
            for (int i = 0; i < strings.length; i++) {
                for (Map.Entry<String, List<String>> entry : coderbook.entrySet()) {
                    for (String line : entry.getValue()) {
                        if (line.equals(strings[i])) {
                            outputDecode += entry.getKey();
                            outputDecode += " ";
                        }
                    }
                }
            }
            return outputDecode;
        } catch (Exception e) {
            System.out.println("Invalid data");
            return "";
        }
    }

    private static String defineLength(String code, int max) {
        String line = "";
        boolean flag = false;
        int j = 0;
        for (int i = 0; i < max - code.length(); i++) {
            if (!flag) {
                line += "0";
                flag = true;
            } else {
                line += code.charAt(j);
                j++;
                flag = false;
            }
        }
        line += code.substring(j);
        return line;
    }

    public static String[] parseString(String line, int codeLength) {
        if (line.length() % codeLength == 0) {
            String[] code = new String[line.length() / codeLength];
            int k = 0;
            int m = codeLength;
            for (int i = 0; i < code.length; i++) {
                code[i] = line.substring(k, m);
                k += codeLength;
                m += codeLength;
            }
            return code;
        } else {
            return null;
        }
    }

}
