package com.vsu.springcriptography.utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.security.*;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.EncodedKeySpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ECDSA {

    private static final String SPEC = "secp256k1";
    private static final String ALGO = "SHA256withECDSA"; // выбранный алгоритм SHA256

    // метод отправителя (Алиса отправляет Бобу сообщение с цифровой подписью)
    private JSONObject sender(String message, String operation) throws NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, UnsupportedEncodingException, SignatureException, JSONException {

        ECGenParameterSpec ecSpec = new ECGenParameterSpec(SPEC);
        KeyPairGenerator g = KeyPairGenerator.getInstance("EC");
        g.initialize(ecSpec, new SecureRandom());
        KeyPair keypair = g.generateKeyPair(); // генерация пары ключей (public и private ключ)
        PublicKey publicKey = keypair.getPublic();
        PrivateKey privateKey = keypair.getPrivate();

        String plaintext = message; // входное сообщение

        Signature ecdsaSign = Signature.getInstance(ALGO); // сигнатура с предложенным алгоритмом
        ecdsaSign.initSign(privateKey);
        ecdsaSign.update(plaintext.getBytes("UTF-8"));
        byte[] signature = ecdsaSign.sign();
        String pub = Base64.getEncoder().encodeToString(publicKey.getEncoded()); // формирования паблик ключа
        String sig = Base64.getEncoder().encodeToString(signature); // формирование сигнатуры
        System.out.println(sig);
        System.out.println(pub);

        JSONObject obj = new JSONObject();
        if (operation.equals("true")) {
            obj.put("publicKey", pub);
        } else {
            obj.put("publicKey", "MFYwEAYHKoZIzj0CAQYFK4EEAAoDQgAEOgtmtq4PitNsiiIE91qS3CWerd9oFKXMBRKXQo4IVYoyiVWwE/NaEAmVIrkO7A3FG0eAlEVfcpUDAIfpF3D82A==");
        }
        obj.put("signature", sig); // поещаем сгенерированную информацию в сообщение
        obj.put("message", plaintext);
        obj.put("algorithm", ALGO);

        return obj;
    }

    // Получатель (Боб принимает сообщение и проверяет полученную цифровую подпись, если Алиса передала верное значение, то будет показано верное сообщение, иначе будет нечитаемый текст)
    private boolean receiver(JSONObject obj) throws NoSuchAlgorithmException, InvalidKeySpecException, InvalidKeyException, UnsupportedEncodingException, SignatureException, JSONException {

        Signature ecdsaVerify = Signature.getInstance(obj.getString("algorithm"));
        KeyFactory kf = KeyFactory.getInstance("EC");

        EncodedKeySpec publicKeySpec = new X509EncodedKeySpec(Base64.getDecoder().decode(obj.getString("publicKey")));

        KeyFactory keyFactory = KeyFactory.getInstance("EC");
        PublicKey publicKey = keyFactory.generatePublic(publicKeySpec);

        ecdsaVerify.initVerify(publicKey);
        ecdsaVerify.update(obj.getString("message").getBytes("UTF-8"));
        boolean result = ecdsaVerify.verify(Base64.getDecoder().decode(obj.getString("signature")));

        return result;
    }

    public String main(String[] args) { // вызовы методов и обработка возможных исключений
        try {
            ECDSA digiSig = new ECDSA();
            JSONObject obj = digiSig.sender(args[0], args[1]);
            boolean result = digiSig.receiver(obj);
            if (result)
                return "true";
            else return "false";
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(ECDSA.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidAlgorithmParameterException ex) {
            Logger.getLogger(ECDSA.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeyException | JSONException ex) {
            Logger.getLogger(ECDSA.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(ECDSA.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SignatureException ex) {
            Logger.getLogger(ECDSA.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(ECDSA.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "";
    }

}
