package com.vsu.springcriptography.utils;

import java.util.Scanner;

public class XorEncryption {

    public String main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter string: ");
        String sampleString = args[0];
        System.out.println("What do you want? - (1) - encrypt, (2) - decrypt");
        String operation = args[1];
        System.out.println("Enter cipher: ");
        String cipher = args[2];
        if (cipher.length() == 1) {
            if (operation.equals("1")) {
                System.out.print("Encrypted string: ");
                return encryptDecrypt(sampleString, cipher.charAt(0));
            } else if (operation.equals("2")) {
                System.out.print("Decrypted string: ");
                return encryptDecrypt(sampleString, cipher.charAt(0));
            } else {
                System.out.println("Invalid operation");
                return "Invalid operation";
            }
        } else {
            System.out.println("Cipher must be character");
            return "Cipher must be character";
        }
    }


    private static String encryptDecrypt(String inputString, char cipher) {
        String outputString = "";
        int len = inputString.length();

        for (int i = 0; i < len; i++) {
            outputString += (char) (inputString.charAt(i) ^ cipher);
        }

        return outputString;
    }

}
