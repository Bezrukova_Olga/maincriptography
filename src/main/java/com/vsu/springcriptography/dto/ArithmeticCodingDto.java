package com.vsu.springcriptography.dto;

public class ArithmeticCodingDto {
    private String operation;
    private String text;
    private Message probability;

    public ArithmeticCodingDto() {
    }

    public ArithmeticCodingDto(String operation, String text, Message probability) {
        this.operation = operation;
        this.text = text;
        this.probability = probability;
    }

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Message getProbability() {
        return probability;
    }

    public void setProbability(Message probability) {
        this.probability = probability;
    }
}
