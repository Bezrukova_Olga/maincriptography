package com.vsu.springcriptography.dto;

import com.vsu.springcriptography.entity.GrantEntity;

import java.util.List;

public class User {
    private String login;
    private List<GrantEntity> grants;

    public User() {
    }

    public User(String login, List<GrantEntity> grants) {
        this.login = login;
        this.grants = grants;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public List<GrantEntity> getGrants() {
        return grants;
    }

    public void setGrants(List<GrantEntity> grants) {
        this.grants = grants;
    }
}
