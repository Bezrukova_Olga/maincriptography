package com.vsu.springcriptography.dto;

import java.util.List;

public class GrantsDto {
    private String user;
    private List<String> grants;

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public GrantsDto() {
    }

    public List<String> getGrants() {
        return grants;
    }

    public void setGrants(List<String> grants) {
        this.grants = grants;
    }
}
