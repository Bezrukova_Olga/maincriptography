package com.vsu.springcriptography.dto;

import java.util.ArrayList;
import java.util.List;

public class GrantList {
    List<String> grantList = new ArrayList<>();

    public GrantList(List<String> grantList) {
        this.grantList = grantList;
    }

    public List<String> getGrantList() {
        return grantList;
    }

    public void setGrantList(List<String> grantList) {
        this.grantList = grantList;
    }
}
