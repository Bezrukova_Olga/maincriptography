package com.vsu.springcriptography.repository;

import com.vsu.springcriptography.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<UserEntity, String> {
    UserEntity findByLogin(String login);
    UserEntity findByPassword(String password);
}
