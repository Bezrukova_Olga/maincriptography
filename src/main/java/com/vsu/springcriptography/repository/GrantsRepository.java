package com.vsu.springcriptography.repository;

import com.vsu.springcriptography.entity.GrantEntity;
import com.vsu.springcriptography.entity.UserEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GrantsRepository extends CrudRepository<GrantEntity, String> {
    GrantEntity findByName(String name);
    List<GrantEntity> findAll();
    List<GrantEntity> findAllByUserEntity(UserEntity user);
}
