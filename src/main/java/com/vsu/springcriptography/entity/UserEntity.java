package com.vsu.springcriptography.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.List;
import java.util.UUID;
import javax.persistence.*;

@Entity
@Table(name = "USERS")
public class UserEntity {
    @Id
    @Column(name = "id_user")
    private String idUser;
    @Column(name = "login")
    private String login;


    @Column(name = "password")
    private String password;

    @OneToMany(mappedBy = "userEntity", cascade = CascadeType.ALL)
    private List<GrantEntity> grants;

    @Column(name = "role")
    private String role;


    public UserEntity() {
    }

    public UserEntity(String login, String password, List<GrantEntity> grants, String role) {
        this.idUser = UUID.randomUUID().toString();
        this.login = login;
        this.password = password;
        this.grants = grants;
        this.role = role;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    @JsonIgnore
    public List<GrantEntity> getGrants() {
        return grants;
    }

    public void setGrants(List<GrantEntity> grants) {
        this.grants = grants;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
