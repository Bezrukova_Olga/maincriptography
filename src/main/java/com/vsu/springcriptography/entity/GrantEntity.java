package com.vsu.springcriptography.entity;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Table(name = "Grants")
public class GrantEntity {

    @Id
    @Column(name = "id_grant")
    private String id;

    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user")
    private UserEntity userEntity;

    public UserEntity getUserEntity() {
        return userEntity;
    }

    public GrantEntity() {
    }

    public void setUserEntity(UserEntity userEntity) {
        this.userEntity = userEntity;
    }

    public GrantEntity(String name, UserEntity userEntity) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.userEntity = userEntity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
