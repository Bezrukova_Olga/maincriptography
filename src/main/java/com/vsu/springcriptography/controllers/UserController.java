package com.vsu.springcriptography.controllers;

import com.vsu.springcriptography.Grants;
import com.vsu.springcriptography.dto.*;
import com.vsu.springcriptography.entity.GrantEntity;
import com.vsu.springcriptography.entity.UserEntity;
import com.vsu.springcriptography.repository.GrantsRepository;
import com.vsu.springcriptography.repository.UserRepository;
import com.vsu.springcriptography.utils.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("")
@CrossOrigin("http://localhost:4200")
public class UserController {

    private User currentUser = null;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private GrantsRepository grantsRepository;


    @GetMapping("/signIn")
    public User signInUser(@RequestParam String login, @RequestParam String password) {
        UserEntity byLogin = userRepository.findByLogin(login);
        if (byLogin != null && byLogin.getPassword().equals(password)) {
            List<GrantEntity> allByUserEntity = grantsRepository.findAllByUserEntity(byLogin);
            User user = new User(byLogin.getLogin(), allByUserEntity);
            currentUser = user;
            return user;
        }
        return null;

    }

    @DeleteMapping("/remove")
    public Message deleteUser(@RequestParam String login) {
        UserEntity byLogin = userRepository.findByLogin(login);
        if (byLogin != null && !byLogin.getLogin().equals("admin")) {
            userRepository.delete(byLogin);
            return new Message("Success");
        } else {
            return new Message("Error");
        }
    }

    @PostMapping("/grants")
    public Message addGrants(@RequestBody GrantsDto grants) {
        for (String gr : grants.getGrants()) {
            System.out.println(gr);
        }
        UserEntity byLogin = userRepository.findByLogin(grants.getUser());
        List<String> nameGrants = new ArrayList<>();
        List<String> listToAdd = new ArrayList<>();
        if (byLogin != null) {
            List<GrantEntity> userGrants = grantsRepository.findAllByUserEntity(byLogin);
            for (GrantEntity entity : userGrants) {
                nameGrants.add(entity.getName());
            }
            for (String add : grants.getGrants()) {
                if (!nameGrants.contains(add)) {
                    listToAdd.add(add);
                }
            }
            if (listToAdd.size() != 0) {
                for (String grant : grants.getGrants()) {
                    userGrants.add(new GrantEntity(grant, byLogin));
                }
                grantsRepository.saveAll(userGrants);
                return new Message("Success");
            } else {
                return new Message("This grants already exist");
            }
        }
        return new Message("Error");
    }

    @GetMapping("/user")
    public User getCurrentUser() {
        return currentUser;
    }

    @PostMapping("/registration")
    public User registration(@RequestBody UserRegistrationDto user) {
        UserEntity byLogin = userRepository.findByLogin(user.getLogin());
        if (byLogin == null) {
            UserEntity byPassword = userRepository.findByPassword(user.getPassword());
            if (byPassword == null) {
                UserEntity userEntity = new UserEntity(user.getLogin(), user.getPassword(), new ArrayList<>(), "User");
                userRepository.save(userEntity);
                return new User(userEntity.getLogin(), userEntity.getGrants());
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @GetMapping("/grasshopper")
    public Message grasshopper(@RequestParam String operation, @RequestParam String text) throws UnsupportedEncodingException {
        String[] array = {operation, text};
        Grasshopper grasshopper = new Grasshopper();
        String main = grasshopper.main(array);
        byte[] bytes = main.getBytes("CP1252");
        String cp866 = new String(main.getBytes(), StandardCharsets.UTF_8);
        System.out.println(cp866);
        String s2 = new String(bytes, "UTF-8");
        return new Message(cp866);
    }

    @GetMapping("/xorEncrypt")
    public Message xorEncrypt(@RequestParam String operation, @RequestParam String text, @RequestParam String cipher) {
        String[] array = {text, operation, cipher};
        XorEncryption xorEncryption = new XorEncryption();
        String main = xorEncryption.main(array);
        String cp866 = new String(main.getBytes(), StandardCharsets.UTF_8);
        System.out.println(cp866);
        return new Message(cp866);
    }

    @GetMapping("/permutationEncrypt")
    public Message permutationEncrypt(@RequestParam String operation, @RequestParam String text, @RequestParam String keyRow, @RequestParam String keyColumn) {
        String[] array = {operation, text, keyRow, keyColumn};
        PermutationEncryption permutationEncrypt = new PermutationEncryption();
        String main = permutationEncrypt.main(array);
        String cp866 = new String(main.getBytes(), StandardCharsets.UTF_8);
        System.out.println(cp866);
        return new Message(main);
    }

    @GetMapping("/coderBook")
    public Message coderBook(@RequestParam String operation, @RequestParam String text) {
        String[] array = {text, operation};
        CoderBook coderBook = new CoderBook();
        String main = coderBook.main(array);
        String cp866 = new String(main.getBytes(), StandardCharsets.UTF_8);
        System.out.println(cp866);
        return new Message(main);
    }

    @GetMapping("/imageCompression")
    public Message imageCompression() throws IOException {
        String[] array = {"stub"};
        Compression imageCompression = new Compression();
        String main = imageCompression.main(array);
        String cp866 = new String(main.getBytes(), StandardCharsets.UTF_8);
        System.out.println(cp866);
        return new Message(main);
    }

    @PostMapping("/arithmeticCoding")
    public Message arithmeticCoding(@RequestBody ArithmeticCodingDto text) throws IOException {
        System.out.println(text.getProbability().getText());
        String[] array = {text.getOperation(), text.getText()};
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("text.txt"));
        bufferedWriter.write(text.getText());
        bufferedWriter.close();
        BufferedWriter probab = new BufferedWriter(new FileWriter("probability.txt"));
        probab.write(text.getProbability().getText());
        probab.close();
        ArithmeticCoding arithmeticCoding = new ArithmeticCoding();
        String main = arithmeticCoding.main(array);
        String cp866 = new String(main.getBytes(), StandardCharsets.UTF_8);
        System.out.println(cp866);
        return new Message(main);
    }

    @GetMapping("/ecdsa")
    public Message ecdsa(@RequestParam String text, @RequestParam String operation) {
        String[] array = {text, operation};
        ECDSA ecdsa = new ECDSA();
        String main = ecdsa.main(array);
        return new Message(main);
    }


}
