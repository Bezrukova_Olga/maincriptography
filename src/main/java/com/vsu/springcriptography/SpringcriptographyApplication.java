package com.vsu.springcriptography;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringcriptographyApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcriptographyApplication.class, args);
	}

}
