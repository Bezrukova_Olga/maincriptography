package com.vsu.springcriptography;

public enum Grants {
    CoderBook, Grasshopper,PermutationEncryption, XorEncryption, ImageWriteParam, Compression
}
